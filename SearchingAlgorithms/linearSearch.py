
def linearSearch1(arr, x):

    for i in range(len(arr)):
        if arr[i] == x:
            return print(x, " değerinin bulunduğu indeks ", i, "\n")
    return print(x, "değeri dizide bulunmamakta!!!!\n")

def linearSearch2(arr, x):

    if x in arr:
        return print(x, "değeri", arr.index(x), "indeksinde bulunuyor. :)\n")
    return print(x, "Değer bulunamadı! :(((\n")


A = [19, 13, 22, 2, 6, 4, 32, 27, 8]

linearSearch1(A, 27)
linearSearch1(A, 3)

linearSearch2(A, 6)
linearSearch2(A, 31)

