
# A: dizi | min: Sıfır | max: Dizinin uzunluğu | x: Bulunması istenen değer

def binarySearchRecursive(A, min, max, x):

    if min <= max:
        mid = min + (max-min)//2

        if A[mid] == x:
            return print(x, " elamanının indeksi ", mid, "\n")
        elif A[mid] > x:
            #Solalt diziyi kontrol et ve max değeri değiştir
            return binarySearchRecursive(A, min, mid-1, x)
        elif A[mid] < x:
            # Sağalt diziyi kontrol et ve min değeri değiştir
            return binarySearchRecursive(A, mid+1, max, x)
    return print("Eleman bulunamadı!\n")

def binarySearchIterative(A, min, max, x):

    while min <= max:

        mid = min + (max-min) //2

        if A[mid] == x:
            print(x, " elamanının indeksi ", mid, "\n")
            return mid

        elif A[mid] > x:
            max = mid-1

        elif A[mid] < x:
            min = mid + 1

    return print("Eleman bulunamadı!")




A = [2, 3, 4, 10, 40, 43, 51, 68, 87]

binarySearchRecursive(A, 0, len(A)-1, 4)
binarySearchRecursive(A, 0, len(A)-1, 51)
binarySearchRecursive(A, 0, len(A)-1, 96)

binarySearchIterative(A, 0, len(A)-1, 10)
binarySearchIterative(A, 0, len(A)-1, 68)
binarySearchIterative(A, 0, len(A)-1, 7)
